'use strict';

var BpmnJS = window.BpmnJS,
    $ = window.jQuery;

var viewer = new BpmnJS({ container: '#diagram' });
function showDiagram(diagramXML) {

  viewer.importXML(diagramXML, function() {
    var eventBus = viewer.get('eventBus');
    var overlays = viewer.get('overlays'),
        canvas = viewer.get('canvas'),
        elementRegistry = viewer.get('elementRegistry');

    eventBus.on('element.click', function (e){
      console.log(event, 'on', e.element);
      canvas.addMarker(e.element, 'highlight');
    });
    // option 1: highlight via CSS class
    canvas.addMarker('OrderReceivedEvent', 'highlight');
  });
}

// load + show diagram
$.get('resources/pizza-collaboration.bpmn', showDiagram);
